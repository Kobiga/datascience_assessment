// importing mongoDB and chalk libraries
const mongodb = require('mongodb');
const chalk= require('chalk');
const  mongoClient=mongodb.MongoClient

const connection_url = "mongodb://localhost:27017"
const databaseName = 'Global_Warming'

mongoClient.connect(connection_url,
    { useNewUrlParser:true,useUnifiedTopology: true },(error, client) => { 
        if (error) {
            return console.log(chalk.red.bold("Unable to connected to MongoDB"))  ;
        }

        const db =  client.db(databaseName)
        if(db){
            console.log(chalk.green.bold(" Database connected Successfully!"));
            console.log("-----------------------------------------------------------");
     
            db.collection('GlobalLandTemperatureByCountry_Details').find({$and:[{$and:[{$and:[{Date:{$gte:'1/1/2000'}},
            {Date:{$lte:'10/1/2000'}}]},{Country:"Syria"}]},{$expr:{$gte:[{$toDouble:"$Value"},20.0]}}]}).toArray(
                (error,GlobalLandTemperatureByCountry_Details) =>{

                    GlobalLandTemperatureByCountry_Details.forEach(function(GlobalLandTemperatureByCountry_Details){
                    console.log(" ");
                    console.log((chalk.cyanBright.bold("Country:")),GlobalLandTemperatureByCountry_Details.Country);
                    console.log((chalk.cyanBright.bold("Date:")),GlobalLandTemperatureByCountry_Details.Date);
                    console.log((chalk.cyanBright.bold("Value:")),GlobalLandTemperatureByCountry_Details.Value);
                    console.log("__________________________________________________________");
            });
             

        } );
       
    }
})
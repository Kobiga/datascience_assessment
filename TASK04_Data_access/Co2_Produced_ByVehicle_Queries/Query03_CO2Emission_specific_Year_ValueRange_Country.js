//importing monodb and chalk libraries
const mongodb = require('mongodb');
const chalk= require('chalk');
const  mongoClient=mongodb.MongoClient

const connection_url = "mongodb://localhost:27017"
const databaseName = 'Global_Warming'

mongoClient.connect(connection_url,
    { useNewUrlParser:true,useUnifiedTopology: true },(error, client) => { 
        if (error) {
            return console.log(chalk.red.bold("Unable to connected to MongoDB"))  ;
        }
        const db =  client.db(databaseName)
        if(db){
            console.log(chalk.green.bold(" Database connected Successfully!"));
            console.log("-----------------------------------------------------------");
           
     
            db.collection('CO2_Produced_From_Vehicle_Details').find({$and:[{$and:[{$and:[{Year:{$gte:'2005'}},
            {Year:{$lte:'2015'}}]},{Country:"Syria"}]},{$expr:{$gte:[{$toDouble:"$CO2_produced_from_vehicle"},
            40000000.0]}}]}).toArray(
               (error,CO2_Produced_From_Vehicle_Details) =>{

                CO2_Produced_From_Vehicle_Details.forEach(function(CO2_Produced_From_Vehicle_Details){
                console.log(" ");
                console.log((chalk.blueBright.bold("Country:")),CO2_Produced_From_Vehicle_Details.Country);
                console.log((chalk.blueBright.bold("Year:")),CO2_Produced_From_Vehicle_Details.Year);
                console.log((chalk.blueBright.bold("Value of CO2:")),CO2_Produced_From_Vehicle_Details.CO2_produced_from_vehicle);
                console.log("__________________________________________________________");
            });
            console.log(" ");
            console.log(chalk.redBright.bold('Syria,in 2008 produced a high amount of CO2 by vehicle '));

        } );
    }
})
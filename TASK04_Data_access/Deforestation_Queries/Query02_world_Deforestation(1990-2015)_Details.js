// importing mongoDB and chalk libraries
const mongodb = require('mongodb');
const chalk= require('chalk');
const  mongoClient=mongodb.MongoClient

const connection_url = "mongodb://localhost:27017"
const databaseName = 'Global_Warming'

mongoClient.connect(connection_url,
    { useNewUrlParser:true,useUnifiedTopology: true },(error, client) => { 
        if (error) {
            return console.log(chalk.red.bold("Unable to connected to MongoDB"))  ;
        }

        const db =  client.db(databaseName)
        if(db){
            console.log(chalk.green.bold(" Database connected Successfully!"));
            console.log("-----------------------------------------------------------");
     
            db.collection('Deforestation_Details').find({$and:[{$and:[{Year:{$gte:'2005'}},
            {Year:{$lte:'2010'}}]},{Country:"Wolrd"}]}).toArray(
                (error,Deforestation_Details) =>{

                        Deforestation_Details.forEach(function(Deforestation_Details){
                        console.log(" ");
                        console.log((chalk.yellowBright.bold("Country:")),Deforestation_Details.Country);
                        console.log((chalk.yellowBright.bold("Year:")),Deforestation_Details.Year);
                        console.log((chalk.yellowBright.bold("Forest Convert to Deforest:")),Deforestation_Details.Net_forest_conversion);
                        console.log("__________________________________________________________");
            });
             

        } );
       
    }
})
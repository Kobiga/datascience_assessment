# import CSV,pymongo
import csv;
import pymongo;


client = pymongo.MongoClient("localhost",27017)
database = client["Global_Warming"]
dbCollections = database["CO2_Produced_From_Vehicle_Details"]

with open('co2_Produced_From_Vehicle.csv')as csvFile:
    readCsv=csv.reader(csvFile,delimiter=',')
    for row in readCsv: 
        data_row = {
            "Country":row[0],
            "Year":row[1],
            "CO2_produced_from_vehicle":row[2]
        }

        result=dbCollections.insert_one(data_row)
        print("The record inserted Country ",row[0])
        print('-------------------------------------')






import csv;
import pymongo;


client = pymongo.MongoClient("localhost",27017)
database = client["Global_Warming"]
dbCollections = database["GlobalLandTemperatureByCountry_Details"]


with open('GlobalLandTemperaturesByCountry.csv')as csvFile:
    readCsv=csv.reader(csvFile,delimiter=',')
    for row in readCsv: 
        data_row = {
            "Country":row[0],
            "Date":row[1],
            "Value":row[2]
        }

        result=dbCollections.insert_one(data_row);
        print("The record inserted Country ",row[0]);
        print('-----------------------------------');
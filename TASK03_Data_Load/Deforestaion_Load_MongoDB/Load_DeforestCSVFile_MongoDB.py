import csv;
import pymongo;


client = pymongo.MongoClient("localhost",27017)
database = client["Global_Warming"]
dbCollections = database["Deforestation_Details"]


with open('annual_change_forest_area .csv')as csvFile:
    readCsv=csv.reader(csvFile,delimiter=',')
    for row in readCsv: 
        data_row = {
            "Country":row[0],
            "Year":row[1],
            "Net_forest_conversion":row[2]
        }

        result=dbCollections.insert_one(data_row);
        print("The record inserted Country ",row[0]);
        print('---------------------');